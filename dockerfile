FROM python:3.7.3-slim
ADD src /deploy
WORKDIR /deploy
RUN pip install -r requirements.txt
CMD ["python", "app.py"]